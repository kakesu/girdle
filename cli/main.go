package main

import (
	"fmt"

	"gitlab.com/kakesu/girdle"
	"golang.org/x/exp/slices"
)

func main() {

	candidateWords := girdle.CommonWords
	combinedWords := append(girdle.AllWords, girdle.CommonWords...)
	letterScores := girdle.GetLetterScores(girdle.CommonWords)
	wordScores := girdle.GetWordScores(combinedWords, letterScores)
	bestWord := girdle.GetBestWord(wordScores)
	knowns := make([]rune, 5)
	var yellowLetters []rune

	for slices.Contains(knowns, 0) {

		fmt.Println("Best candidate word is", bestWord)
		results := ""
		fmt.Println("Enter results:")
		fmt.Scanln(&results)
		girdle.UpdateLetterScores(letterScores, bestWord, results, &knowns, &candidateWords, &yellowLetters)
		girdle.UpdateWordScores(wordScores, letterScores)
		bestWord = girdle.GetBestWord(wordScores)

	}

}
