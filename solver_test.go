package girdle

import (
	"fmt"
	"reflect"
	"testing"
)

func getTestWordList() []string {
	return []string{
		"alpha",
		"apple",
		"bravo",
		"delta",
		"hello",
	}
}

func TestGetLetterScores(t *testing.T) {

	get := GetLetterScores(getTestWordList())

	want := make(map[rune][]int)

	for i := 'a'; i <= 'z'; i++ {
		zeroSlice := make([]int, 5)
		want[i] = zeroSlice
	}

	want['a'] = []int{2, 0, 1, 0, 2}
	want['b'] = []int{1, 0, 0, 0, 0}
	want['d'] = []int{1, 0, 0, 0, 0}
	want['e'] = []int{0, 2, 0, 0, 1}
	want['h'] = []int{1, 0, 0, 1, 0}
	want['l'] = []int{0, 1, 2, 2, 0}
	want['o'] = []int{0, 0, 0, 0, 2}
	want['p'] = []int{0, 1, 2, 0, 0}
	want['r'] = []int{0, 1, 0, 0, 0}
	want['t'] = []int{0, 0, 0, 1, 0}
	want['v'] = []int{0, 0, 0, 1, 0}

	if !reflect.DeepEqual(get, want) {
		fmt.Println(get)
		t.Error("Letter scores do not match expected values")
	}

}

func TestGetWordScores(t *testing.T) {

	var testCases = []struct {
		word []string
		want int
	}{
		{[]string{"alpha"}, 8},
		{[]string{"apple"}, 8},
		{[]string{"bravo"}, 6},
		{[]string{"delta"}, 8},
		{[]string{"hello"}, 9},
	}

	letterScores := GetLetterScores(getTestWordList())

	for _, testCase := range testCases {
		testName := fmt.Sprintf(testCase.word[0])
		t.Run(testName, func(t *testing.T) {
			get := GetWordScores(testCase.word, letterScores)
			if get[testCase.word[0]] != testCase.want {
				t.Errorf("got %d, want %d", get[testCase.word[0]], testCase.want)
			}
		})
	}

}

func TestGetBestWord(t *testing.T) {
	scoredWordList := GetWordScores(getTestWordList(), GetLetterScores(getTestWordList()))

	get := GetBestWord(scoredWordList)
	want := "hello"

	if get != want {
		t.Errorf("got %s, want %s", get, want)
	}
}

func TestUpdateLetterScores(t *testing.T) {
	candidateWords := getTestWordList()
	letterScores := GetLetterScores(getTestWordList())
	word := "ralaa" // real answer is "alpha"
	colors := "BYYBG"
	knowns := make([]rune, 5)
	var yellowLetters []rune

	UpdateLetterScores(letterScores, word, colors, &knowns, &candidateWords, &yellowLetters)

	wantLetterScores := make(map[rune][]int)
	for i := 'a'; i <= 'z'; i++ {
		zeroSlice := make([]int, 5)
		wantLetterScores[i] = zeroSlice
	}
	wantLetterScores['a'] = []int{2, 0, 0, 0, 2}
	wantLetterScores['l'] = []int{0, 2, 0, 0, 0}
	wantLetterScores['p'] = []int{0, 0, 1, 0, 0}
	wantLetterScores['h'] = []int{0, 0, 0, 1, 0}

	wantKnowns := []rune{0, 0, 0, 0, 'a'}

	if !reflect.DeepEqual(wantLetterScores, letterScores) {
		t.Error("Letter scores do not match expected values")
		t.Log("Want:\n", wantLetterScores)
		t.Log("Got:\n", letterScores)
	}

	if !reflect.DeepEqual(wantKnowns, knowns) {
		t.Error("Knowns do not match expected values")
	}

}
