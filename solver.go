package girdle

import "golang.org/x/exp/slices"

func GetBestWord(scoredWordList map[string]int) string {
	bestWord := ""
	bestScore := 0

	for word, score := range scoredWordList {
		if score > bestScore {
			bestScore = score
			bestWord = word
		}
	}

	return bestWord
}

func GetLetterScores(wordlist []string) map[rune][]int {
	letterScores := make(map[rune][]int)

	for i := 'a'; i <= 'z'; i++ {
		zeroSlice := make([]int, 5)
		letterScores[i] = zeroSlice
	}

	for _, word := range wordlist {
		for j, letter := range word {
			letterScores[letter][j]++
		}
	}

	return letterScores
}

func GetWordScores(wordList []string, letterScores map[rune][]int) map[string]int {
	wordScores := make(map[string]int)

	for _, word := range wordList {
		wordScore := 0
		for j, letter := range word {
			wordScore += letterScores[letter][j]
		}
		wordScores[word] = wordScore
	}

	return wordScores
}

func UpdateLetterScores(letterScores map[rune][]int, word string, colors string, knowns *[]rune, candidateWords *[]string, yellowLetters *[]rune) {

	// Letters need to be processed in the order green > yellow > black
	for i, letter := range word {
		color := colors[i]
		if (*knowns)[i] != 0 {
			continue
		}

		switch color {
		case 'G', 'g':
			for j := 'a'; j <= 'z'; j++ {
				if j != letter {
					letterScores[j][i] = 0
				}
			}
			(*knowns)[i] = letter
			for i := 0; i < len(*yellowLetters); i++ {
				if (*yellowLetters)[i] == letter {
					(*yellowLetters)[i] = (*yellowLetters)[len(*yellowLetters)-1]
					*yellowLetters = (*yellowLetters)[:len(*yellowLetters)-1]
				}
			}
		}
	}

	for i, letter := range word {
		color := colors[i]

		switch color {
		case 'Y', 'y':
			letterScores[letter][i] = 0
			// Probably want something here to indicate a known letter in an unknown space?
			// Not sure what that would be though, might need to test some options.
			if !slices.Contains(*yellowLetters, letter) {
				*yellowLetters = append(*yellowLetters, letter)
			}
		}
	}

	for i, letter := range word {
		color := colors[i]

		switch color {
		case 'B', 'b':
			if slices.Contains(*yellowLetters, letter) {
				letterScores[letter][i] = 0
				break
			}
			for j := 0; j < len(word); j++ {
				if (*knowns)[j] != letter {
					letterScores[letter][j] = 0
				}
			}
		}
	}

	updateCandidateWords(letterScores, knowns, candidateWords, yellowLetters)

	newLetterScores := GetLetterScores(*candidateWords)

	for letter := range newLetterScores {
		for i := 0; i < len(newLetterScores[letter]); i++ {
			if newLetterScores[letter][i] < letterScores[letter][i] {
				letterScores[letter][i] = newLetterScores[letter][i]
			}
		}
	}

	for _, letter := range *yellowLetters {
		for i := 0; i < len(letterScores[letter]); i++ {
			letterScores[letter][i] = 2 * letterScores[letter][i]
		}
	}

}

func updateCandidateWords(letterScores map[rune][]int, knowns *[]rune, candidateWords *[]string, yellowLetters *[]rune) {
	newCandidateWords := (*candidateWords)[:0]
	for _, word := range *candidateWords {
		removeWord := false
		for j, knownLetter := range *knowns {
			if knownLetter != 0 && knownLetter != []rune(word)[j] {
				removeWord = true
				break
			}
			if letterScores[[]rune(word)[j]][j] == 0 {
				removeWord = true
				break
			}
		}

		if !removeWord && len(*yellowLetters) > 0 {
			for _, letter := range *yellowLetters {
				containsLetter := false
				for _, wordLetter := range word {
					if letter == wordLetter {
						containsLetter = true
					}
				}
				if !containsLetter {
					removeWord = true
					break
				}
			}
		}

		if !removeWord {
			newCandidateWords = append(newCandidateWords, word)
		}
	}
	for i := len(newCandidateWords); i < len(*candidateWords); i++ {
		(*candidateWords)[i] = ""
	}
	(*candidateWords) = newCandidateWords

}

func UpdateWordScores(wordScores map[string]int, letterScores map[rune][]int) {
	for word := range wordScores {
		newScore := 0
		for j, letter := range word {
			newScore += letterScores[letter][j]
		}
		if newScore == 0 {
			delete(wordScores, word)
		} else {
			wordScores[word] = newScore
		}

	}
}
